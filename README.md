This _openFrameworks_ project creates tangents and normals over mouse movements
or Wacom tablet sketches. It also creates new sketches that mimic the actions of
the mouse, with slight modifications.

It is a simple usage of `ofPolyline` class. This class either takes the `x/y`
coordinates of the mouse and stores them as vertices to be drawn by `openGL` or
interpolates between them first to form lines or bezier curves.

The code in this repo simply adds tangents for some points (drawing for all
would be redundant) and also normals over others.

Using `ofNode` class, which is an abstraction over `openGL` camera functions, I
have created hierarchies over individual points. These hierarchies accept
transformation matricies which are applied to all the `child nodes` when
requested.

The improviser is simply a method where the mouse co-ordinates are stored in an
array with small modifications over the co-ordinates. Using the node
relationship mentioned above, transformations are also applied over these arrays
to draw them using different tilt and orientation.
