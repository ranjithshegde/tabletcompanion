#include "ofApp.h"
#include "ofGraphics.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    sampleRate = 48000;
    bufferSize = 512;
    minDistance = 10;
    leftMouseButtonPressed = false;
    cam.disableMouseInput();
    mouseWorld = false;

    baseNode.setPosition(0, 0, 0);
    childNode.setParent(baseNode);
    childNode.setPosition(0, 0, 200);
    grandChildNode.setParent(childNode);
    grandChildNode.setPosition(0, 50, 0);
    delay = 180;
    erase = delay * 3;
    watch.start();
    tilt = 0;
    oldTime = 0;
}

//--------------------------------------------------------------
void ofApp::update()
{
    if (leftMouseButtonPressed) {
        ofPoint mousePos(ofGetMouseX(), ofGetMouseY());
        if (lastPoint.distance(mousePos) >= minDistance) {
            currentPolyline.curveTo(mousePos);
            lastPoint = mousePos;
        }
    }

    baseNode.tiltDeg(newTime);
    childNode.tiltDeg(3 + newTime);

    baseNode.setPosition(mouseX, mouseY, 0);

    nodeVerts.push_back(grandChildNode.getGlobalPosition() + glm::vec2(newTime, newTime));

    if (nodeVerts.size() >= delay) {
        line.addVertex(nodeVerts[delay]);
    }
    if (line.size() > 200) {
        line.getVertices().erase(line.getVertices().begin());
    }
    if (nodeVerts.size() > erase) {
        nodeVerts.erase(nodeVerts.begin());
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    if (isSavingPdf) {
        ofBeginSaveScreenAsPDF("savedScreenshot_" + ofGetTimestampString() + ".pdf");
    }

    if (mouseWorld) {
        cam.enableMouseInput();
    } else {
        cam.disableMouseInput();
    }

    if (undoButton) {
        if (polylines.size() > 0) {

            polylines.erase(polylines.end());
            undoButton = false;
        } else {
            polylines.clear();
        }
    }
    if (redoButton) {
        polylines = polyBackup;
        redoButton = false;
    }
    if (mouseWorld) {
        cam.begin();
    }
    ofSetColor(255);
    for (int i = 0; i < polylines.size(); i++) {
        ofPolyline polyline = polylines[i];
        polyline.draw();

        ofSetColor(255, 50);
        float tangentLength = 150;
        for (int p = 0; p < 250; p += 1) {
            ofVec3f point = polyline.getPointAtPercent(p / 250.0);
            float floatIndex = polyline.getIndexAtPercent(p / 250.0);
            ofVec3f tangent = polyline.getTangentAtIndexInterpolated(floatIndex) * tangentLength;
            ofDrawLine(point - tangent / 2, point + tangent / 2);
        }
    }
    ofSetColor(255, 100, 0);
    currentPolyline.draw();

    if (isSavingPdf) {
        ofEndSaveScreenAsPDF();
        isSavingPdf = false;
    }

    childNode.draw();
    grandChildNode.draw();

    // ofPushMatrix();
    // ofBackground(0);
    // ofRotateDeg(tilt);
    line.draw();
    if (mouseWorld) {
        cam.end();
    }
    // ofPopMatrix();
}
//--------------------------------------------------------------
void ofApp::audioOut(float* buffer, int bufferSize, int nChannels)
{
    for (int i = 0; i < bufferSize; i++) {
        float currentSample = 0;

        buffer[i * nChannels + 0] = currentSample;
        buffer[i * nChannels + 1] = currentSample;
    }
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if (key == 'u') {
        undoButton = true;
    }
    if (key == 'r') {
        redoButton = true;
    }
    if (key == 's') {
        isSavingPdf = true;
    }

    if (key == 'e') {
        mouseWorld = !mouseWorld;
    }
    if (key == 'c') {
        polylines.clear();
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    if (!mouseWorld) {
        if (button == OF_MOUSE_BUTTON_LEFT) {
            watch.restart();
            leftMouseButtonPressed = true;
            currentPolyline.curveTo(x, y);
            currentPolyline.curveTo(x, y);
            lastPoint.set(x, y);
        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
    if (!mouseWorld) {
        if (button == OF_MOUSE_BUTTON_LEFT) {
            leftMouseButtonPressed = false;
            currentPolyline.curveTo(x, y);
            currentPolyline.simplify(0.75);

            polylines.push_back(currentPolyline);
            polyBackup.push_back(currentPolyline);
            currentPolyline.clear();

            newTime = watch.elapsedSeconds();
            if (newTime > 6) {
                tilt += 9;
            }
            watch.stop();
        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg)
{
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
}
//--------------------------------------------------------------
void ofApp::exit() { soundStream.close(); }
