#pragma once

#include "Poco/Stopwatch.h"
#include "ofMain.h"
#include <vector>

class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();

    void audioOut(float* buffer, int bufferSize, int nChannels);
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    ofSoundStream soundStream;
    int bufferSize;
    int sampleRate;

    ofPolyline currentPolyline;
    bool leftMouseButtonPressed;
    ofVec2f lastPoint;
    float minDistance;
    vector<ofPolyline> polylines;
    vector<ofPolyline> polyBackup;

    bool isSavingPdf;
    bool undoButton;
    bool redoButton;

    ofNode baseNode;
    ofNode childNode;
    ofNode grandChildNode;
    vector<glm::vec3> nodeVerts;
    int delay, erase;
    ofPolyline line;
    ofEasyCam cam;
    bool mouseWorld;

    float newTime, oldTime;
    Poco::Stopwatch watch;
    int tilt;
};
